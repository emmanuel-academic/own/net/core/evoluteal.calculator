﻿using Evoluteal.Calculator.Business.Domain.Service;
using Evoluteal.Calculator.Business.Repository;
using Evoluteal.Calculator.Business.UsesCases;
using Evoluteal.Calculator.Common.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Evoluteal.Calculator.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalculatorController : ControllerBase
    {
        private readonly IOperationRepository _operationRepository;

        public CalculatorController(IOperationRepository operationRepository)
        {
            this._operationRepository = operationRepository;
        }

        #region add
        [Route("add")]
        [HttpPost] //Always explicitly state the accepted HTTP method
        public IActionResult Add([FromBody]RootAddRequest rootRequest)
        {
            try
            {
                string xEviTrackingId = this.Request.Headers["XEviTrackingId"];

                if (string.IsNullOrEmpty(xEviTrackingId))
                {
                    return this.StatusCode(StatusCodes.Status400BadRequest,
                        new ProcessResponse()
                        {
                            Message = "No existe la cabecera XEviTrackingId",
                        });
                }
                else
                {
                    Add addUseCase = new Add(this._operationRepository)
                    {
                        AddRequest = rootRequest,
                        XEviTrackingId = xEviTrackingId
                    };

                    return this.StatusCode(StatusCodes.Status200OK, addUseCase.Execute());
                }
            }

            catch (EvolutealException ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    new ProcessResponse()
                    {
                        IsSuccessful = false,
                        Class = ex.Class,
                        Code = ex.Code,
                        Message = ex.Message,
                        Method = ex.Method
                    });
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    new ProcessResponse()
                    {
                        IsSuccessful = false,
                        Class = this.GetType().FullName,
                        Message = ex.Message,
                        Method = System.Reflection.MethodBase.GetCurrentMethod().Name
                    });
            }

        }
        #endregion
    }
}