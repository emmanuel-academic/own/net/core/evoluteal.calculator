﻿using Evoluteal.Calculator.Business.Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Evoluteal.Calculator.Test.Calculator
{
    [TestClass]
    public class SumOperationStrategyUnitTest
    {

        public SumOperationStrategyUnitTest()
        {
        }

        private void Add(List<double> Addends, double expected)
        {
            //Arrange 
            ContextOperation context = new ContextOperation();

            //Act
            double operationValue = context.Sum(Addends);

            //Assert 
            Assert.AreEqual(expected.ToString(), operationValue.ToString());
        }

        [TestMethod]
        public void Add1() => this.Add(new List<double>() { 1, 2, 3, 5 }, 11);

        [TestMethod]
        public void Add2() => this.Add(new List<double>() { 1, 2, 3, 5, 20 }, 31);

        [TestMethod]
        public void Add3() => this.Add(new List<double>() { 1, 2, 3, 5, 4 }, 15);

        [TestMethod]
        public void Add4() => this.Add(new List<double>() { 11, 2, 30, 5, 500 }, 548);

        [TestMethod]
        public void Add5() => this.Add(new List<double>() { 11.5, 2, 30.7, 5, 500 }, 549.2);

        [TestMethod]
        public void Add6() => this.Add(new List<double>() { 11.5, 2, 30.7, -5, 500 }, 539.2);
    }
}
