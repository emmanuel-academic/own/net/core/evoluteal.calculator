﻿using Evoluteal.Calculator.Business.Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Evoluteal.Calculator.Test.Calculator
{
    [TestClass]
    public class DiffOperationStrategyUnitTest
    {

        public DiffOperationStrategyUnitTest()
        {
        }

        private void Diff(double Minuend, double Subtrahend, double expected)
        {
            //Arrange 
            ContextOperation context = new ContextOperation();

            //Act
            double operationValue = context.Diff(Minuend, Subtrahend);

            //Assert 
            Assert.AreEqual(expected.ToString(), operationValue.ToString());
        }


        [TestMethod]
        public void Sub1() => this.Diff(30, 10, 20);

        [TestMethod]
        public void Sub2() => this.Diff(30, 15, 15);

        [TestMethod]
        public void Sub3() => this.Diff(20, 30, -10);

        [TestMethod]
        public void Sub4() => this.Diff(50.2, 30.5, 19.7);

        [TestMethod]
        public void Sub5() => this.Diff(-50, -30.5, -19.5);
    }

}
