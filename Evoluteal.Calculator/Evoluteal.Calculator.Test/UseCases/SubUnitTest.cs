using Evoluteal.Calculator.Business.Domain.Service;
using Evoluteal.Calculator.Business.Domain.TO;
using Evoluteal.Calculator.Business.Repository;
using Evoluteal.Calculator.Business.UsesCases;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Evoluteal.Calculator.Test.UsesCases
{
    [TestClass]
    public class SubUnitTest
    {
        private readonly Mock<IOperationRepository> repositoryMock;

        public SubUnitTest()
        {
            this.repositoryMock = new Mock<IOperationRepository>();
        }

        private void Subtract(double Minuend, double Subtrahend, string XEviTrackingId, double expected)
        {
            //Arrange 
            repositoryMock.Setup(x => x.PersistOperation(It.IsAny<OperationTO>())).Returns(new OperationTO());
            var operation = new Sub(this.repositoryMock.Object)
            {
                SubRequest = new RootSubRequest() { Minuend = Minuend, Subtrahend = Subtrahend },
                XEviTrackingId = XEviTrackingId
            };

            //Act
            double operationValue = operation.Execute().Difference;

            //Assert 
            Assert.AreEqual(expected.ToString(), operationValue.ToString());
        }

        [TestMethod]
        public void Sub1() => this.Subtract(30, 10, "12456", 20);

        [TestMethod]
        public void Sub2() => this.Subtract(30, 15, "12456", 15);

        [TestMethod]
        public void Sub3() => this.Subtract(20, 30, "12456", -10);

        [TestMethod]
        public void Sub4() => this.Subtract(50.2, 30.5, "12456", 19.7);

    }
}
