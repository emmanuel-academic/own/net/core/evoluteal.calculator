using Evoluteal.Calculator.Business.Domain.Service;
using Evoluteal.Calculator.Business.Domain.TO;
using Evoluteal.Calculator.Business.Repository;
using Evoluteal.Calculator.Business.UsesCases;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace Evoluteal.Calculator.Test.UsesCases
{
    [TestClass]
    public class AddUnitTest
    {
        private readonly Mock<IOperationRepository> repositoryMock;

        public AddUnitTest()
        {
            this.repositoryMock = new Mock<IOperationRepository>();
        }

        private void Add(List<double> Addends, string XEviTrackingId, double expected)
        {
            //Arrange 
            repositoryMock.Setup(x => x.PersistOperation(It.IsAny<OperationTO>())).Returns(new OperationTO());
            var operation = new Add(this.repositoryMock.Object)
            {
                AddRequest = new RootAddRequest() { Addends = Addends },
                XEviTrackingId = XEviTrackingId
            };

            //Act
            double operationValue = operation.Execute().Sum;

            //Assert 
            Assert.AreEqual(expected.ToString(), operationValue.ToString());
        }

        [TestMethod]
        public void Add1() => this.Add(new List<double>() { 1, 2, 3, 5 }, "12456", 11);

        [TestMethod]
        public void Add2() => this.Add(new List<double>() { 1, 2, 3, 5, 20 }, "12456", 31);

        [TestMethod]
        public void Add3() => this.Add(new List<double>() { 1, 2, 3, 5, 4 }, "12456", 15);

        [TestMethod]
        public void Add4() => this.Add(new List<double>() { 11, 2, 30, 5, 500 }, "12456", 548);

        [TestMethod]
        public void Add5() => this.Add(new List<double>() { 11.5, 2, 30.7, 5, 500 }, "12456", 549.2);

        [TestMethod]
        public void Add6() => this.Add(new List<double>() { 11.5, 2, 30.7, -5, 500 }, "12456", 539.2);
    }
}
