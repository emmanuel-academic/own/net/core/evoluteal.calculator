﻿namespace Evoluteal.Calculator.Common.Infrastructure
{
    ///<summary>Clase que realiza la trazabiliad y la respuesta de los procesos.</summary>
    ///
    public class ProcessResponse
    {
        ///<summary>Método constructor por defecto.</summary>
        public ProcessResponse()
        {
            this.IsSuccessful = false;
            this.Message = string.Empty;
            this.Class = string.Empty;
            this.Method = string.Empty;
            this.Code = string.Empty;
        }

        ///<summary>Método constructor.</summary>
        ///<param name="EsExitoso">Objeto de tipo Boolean.</param> 
        ///<param name="Mensaje">Objeto de tipo string.</param> 
        public ProcessResponse(bool IsSuccessfulP, string MessageP)
        {
            this.IsSuccessful = IsSuccessfulP;
            this.Message = MessageP;
        }

        ///<summary>Propiedades get y set para el atributo Mensaje de tipo string.</summary>
        ///
        public string Message { get; set; }

        ///<summary>Propiedades get y set para el atributo Metodo de tipo string.</summary>
        public string Method { get; set; }

        ///<summary>Propiedades get y set para el atributo Clase de tipo string.</summary>
        public string Class { get; set; }

        ///<summary>Propiedades get y set para el atributo EsExitoso de tipo Boolean.</summary>
        public bool IsSuccessful { get; set; }

        ///<summary>Propiedades get y set para el atributo Code de tipo string.</summary>
        public string Code { get; set; }
    }
}
