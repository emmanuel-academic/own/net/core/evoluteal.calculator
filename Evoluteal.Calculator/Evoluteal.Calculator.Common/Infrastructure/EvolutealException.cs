﻿using System;

namespace Evoluteal.Calculator.Common.Infrastructure
{
    ///<summary>Clase que hereda la clase Exception, está se usá para controlar las excepciones.</summary>
    public class EvolutealException : Exception
    {
        ///<summary>Método constructor.</summary>
        ///<param name="strMensajeP">Objeto de tipo string.</param> 
        ///<param name="objTrazaP">Objeto de tipo Exception.</param> 
        ///<param name="strClaseP">Objeto de tipo string.</param> 
        ///<param name="strMetodoP">Objeto de tipo string.</param> 
        public EvolutealException(string strMessageP, Exception objTraceP, string strClassP, string strMethodP)
            : base(strMessageP, objTraceP)
        {
            this.Method = strMethodP;
            this.Class = strClassP;
        }

        ///<summary>Método constructor.</summary>
        ///<param name="strMensajeP">Objeto de tipo string.</param> 
        ///<param name="objTrazaP">Objeto de tipo Exception.</param> 
        ///<param name="strClaseP">Objeto de tipo string.</param> 
        ///<param name="strMetodoP">Objeto de tipo string.</param> 
        public EvolutealException(string strMessageP, Exception objTraceP, string strClassP, string strMethodP, string strCodeP)
            : base(strMessageP, objTraceP)
        {
            this.Method = strMethodP;
            this.Class = strClassP;
            this.Code = strCodeP;
        }

        public EvolutealException(string strMessageP, Exception objTraceP)
           : base(strMessageP, objTraceP)
        {
        }

        public EvolutealException()
        {
        }

        ///<summary>Propiedades get y set para el atributo Traza de tipo Exception.</summary>
        public Exception Trace
        {
            get { return this.InnerException; }
        }

        ///<summary>Propiedades get y set para el atributo Clase de tipo string.</summary>
        public string Class { get; }

        ///<summary>Propiedades get y set para el atributo Metodo de tipo string.</summary>
        public string Method { get; }

        ///<summary>Propiedades get y set para el atributo Codigo de tipo string.</summary>
        public string Code { get; }
    }
}
