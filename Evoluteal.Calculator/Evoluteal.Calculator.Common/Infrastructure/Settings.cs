﻿namespace Evoluteal.Calculator.Common.Infrastructure
{
    public class Settings
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}
