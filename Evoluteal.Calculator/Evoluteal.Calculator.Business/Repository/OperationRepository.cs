﻿using Evoluteal.Calculator.Business.Domain.TO;
using Evoluteal.Calculator.Common.Infrastructure;
using Microsoft.Extensions.Options;
using RepositoryData.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Evoluteal.Calculator.Business.Repository
{
    public class OperationRepository : IOperationRepository
    {
        private readonly IOptions<Settings> _settings;

        public OperationRepository(IOptions<Settings> settings)
        {
            this._settings = settings;
        }
        public OperationTO PersistOperation(OperationTO operationDTO)
        {
            try
            {
                using (JournalContext _dbContext = new JournalContext(this._settings))
                {
                    _dbContext.Operations.Add(OperationTO_OP.ToEntity(operationDTO));
                    _dbContext.SaveChanges();
                    return operationDTO;
                }
            }
            catch (Exception ex)
            {
                throw new EvolutealException(ex.Message, ex.InnerException, this.GetType().FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }
        public List<OperationTO> GetOperationsById(string id)
        {
            try
            {
                using (JournalContext _dbContext = new JournalContext(this._settings))
                {
                    List<TblOperation> operations = _dbContext.Operations.Where(obj => obj.Id == id).ToList();
                    return OperationTO_OP.ToDTOs(operations);
                }
            }
            catch (Exception ex)
            {
                throw new EvolutealException(ex.Message, ex.InnerException, this.GetType().FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
