﻿using Evoluteal.Calculator.Business.Domain.TO;
using System.Collections.Generic;

namespace Evoluteal.Calculator.Business.Repository
{
    public interface IOperationRepository
    {
        OperationTO PersistOperation(OperationTO operationDTO);
        List<OperationTO> GetOperationsById(string id);
    }
}
