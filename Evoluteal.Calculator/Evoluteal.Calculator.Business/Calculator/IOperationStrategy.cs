﻿namespace Evoluteal.Calculator.Business.Calculator
{
    public interface IOperationStrategy
    {
        string Name { get; }
        string OperatorCode { get; }
    }
}
