﻿using System.Collections.Generic;

namespace Evoluteal.Calculator.Business.Calculator
{
   public interface IMultipleArgsOperationStrategy :IOperationStrategy
    {
        double Calculate(List<double> argumentList);
    }
}
