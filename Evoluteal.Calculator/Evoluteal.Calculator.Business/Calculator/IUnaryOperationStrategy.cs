﻿namespace Evoluteal.Calculator.Business.Calculator
{
   public interface IUnaryOperationStrategy : IOperationStrategy
    {
        double Calculate(double argument);
    }
}
