﻿namespace Evoluteal.Calculator.Business.Calculator
{
    public class DivisionOperationStrategy : IBinaryOperationStrategy
    {
        public string Name => "diff";

        public string OperatorCode => "/";

        public double Calculate(double argument1, double argument2, out double argument3)
        {
            if (argument2 == 0)
            {
                argument3 = double.NaN;
                return double.NaN;
            }
            argument3 = argument1 % argument2;
            return argument1 / argument2;
        }
    }
}
