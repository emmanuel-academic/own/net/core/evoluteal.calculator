﻿using Evoluteal.Calculator.Business.Calculator;
using Evoluteal.Calculator.Business.Domain.Service;
using Evoluteal.Calculator.Business.Repository;

namespace Evoluteal.Calculator.Business.UsesCases
{
    public class Sub
    {
        public Sub(IOperationRepository operationRepository)
        {
        }

        public RootSubRequest SubRequest { get; set; }
        public string XEviTrackingId { get; set; }

        public RootSubResponse Execute()
        {
            ContextOperation operation = new ContextOperation();

            RootSubResponse objResponse = new RootSubResponse()
            {
                Difference = operation.Diff(this.SubRequest.Minuend, this.SubRequest.Subtrahend)
            };

            return objResponse;
        }
    }
}

