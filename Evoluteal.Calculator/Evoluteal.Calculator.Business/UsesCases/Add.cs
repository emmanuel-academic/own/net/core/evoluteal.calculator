﻿using System;
using Evoluteal.Calculator.Business.Calculator;
using Evoluteal.Calculator.Business.Domain.TO;
using Evoluteal.Calculator.Business.Domain.Service;
using Evoluteal.Calculator.Business.Repository;
using Evoluteal.Calculator.Common.Infrastructure;

namespace Evoluteal.Calculator.Business.UsesCases
{
    public class Add
    {
        private IOperationRepository OperationRepository { get; set; }
        public RootAddRequest AddRequest { get; set; }
        public string XEviTrackingId { get; set; }

        public Add(IOperationRepository operationRepository)
        {
            this.OperationRepository = operationRepository;
        }

        public RootAddResponse Execute()
        {
            try
            {
                ContextOperation context = new ContextOperation();

                RootAddResponse rootResponse = new RootAddResponse()
                {
                    Sum = context.Sum(AddRequest.Addends)
                };

                OperationTO operation = new OperationTO()
                {
                    Calculation = string.Join(context.MultipleArgsOperationStrategy.OperatorCode, AddRequest.Addends) + "=" + rootResponse.Sum,
                    Id = XEviTrackingId,
                    Date = DateTime.Now,
                    OperationName = context.MultipleArgsOperationStrategy.Name
                };

                this.OperationRepository.PersistOperation(operation);

                return rootResponse;
            }
            catch (EvolutealException ex)
            {
                throw new EvolutealException(ex.Message, ex.InnerException, ex.Class, ex.Method);
            }
            catch (Exception ex)
            {
                throw new EvolutealException(ex.Message, ex.InnerException, this.GetType().FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            
        }
    }
}
