﻿using System;
using System.Runtime.Serialization;

namespace Evoluteal.Calculator.Business.Domain.TO
{
    [DataContract()]
    public partial class OperationTO
    {
        [DataMember()]
        public int IdPK { get; set; }
        [DataMember()]
        public string Id { get; set; }
        [DataMember()]
        public string OperationName { get; set; }
        [DataMember()]
        public string Calculation { get; set; }
        [DataMember()]
        public DateTime Date { get; set; }
    }
}
