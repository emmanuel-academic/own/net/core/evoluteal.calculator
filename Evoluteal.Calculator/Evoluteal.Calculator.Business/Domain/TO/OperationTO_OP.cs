﻿using RepositoryData.Models;
using System.Linq;
using System.Collections.Generic;

namespace Evoluteal.Calculator.Business.Domain.TO
{
    public static partial class OperationTO_OP
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="CustomerDTO"/> converted from <see cref="Customer"/>.</param>
        static partial void OnDTO(this TblOperation entity, OperationTO dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Customer"/> converted from <see cref="CustomerDTO"/>.</param>
        static partial void OnEntity(this OperationTO dto, TblOperation entity);

        /// <summary>
        /// Converts this instance of <see cref="CustomerDTO"/> to an instance of <see cref="Customer"/>.
        /// </summary>
        /// <param name="dto"><see cref="CustomerDTO"/> to convert.</param>
        public static TblOperation ToEntity(this OperationTO dto)
        {
            if (dto is null)
            {
                return null;
            }

            var entity = new TblOperation();

            entity.Id = dto.Id;
            entity.IdPK = dto.IdPK;
            entity.Operation = dto.OperationName;
            entity.Calculation = dto.Calculation;
            entity.Date = dto.Date;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Customer"/> to an instance of <see cref="CustomerDTO"/>.
        /// </summary>
        /// <param name="entity"><see cref="Customer"/> to convert.</param>
        public static OperationTO ToDTO(this TblOperation entity)
        {
            if (entity is null)
            {
                return null;
            }

            var dto = new OperationTO();

            dto.Id = entity.Id;
            dto.IdPK = entity.IdPK;
            dto.OperationName = entity.Operation;
            dto.Calculation = entity.Calculation;
            dto.Date = entity.Date;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="CustomerDTO"/> to an instance of <see cref="Customer"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<TblOperation> ToEntities(this IEnumerable<OperationTO> dtos)
        {
            if (dtos is null)
            {
                return new List<TblOperation>();
            }

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Customer"/> to an instance of <see cref="CustomerDTO"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<OperationTO> ToDTOs(this IEnumerable<TblOperation> entities)
        {
            if (entities is null)
            {
                return new List<OperationTO>();
            }

            return entities.Select(e => e.ToDTO()).ToList();
        }
    }
}
