﻿namespace Evoluteal.Calculator.Business.Domain.Service
{
    public class RootSqrtResponse
    {
        public double Square { get; set; }
    }
}
