﻿namespace Evoluteal.Calculator.Business.Domain.Service
{
    public class RootAddResponse
    {
        public double Sum { get; set; }
    }
}
