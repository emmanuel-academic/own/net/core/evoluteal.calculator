﻿namespace Evoluteal.Calculator.Business.Domain.Service
{
    public class RootSqrtRequest
    {
        public double Number { get; set; }
    }
}
