﻿namespace Evoluteal.Calculator.Business.Domain.Service
{
    public class RootDivRequest
    {
        public double Dividend { get; set; }
        public double Divisor { get; set; }
    }
}
