﻿namespace Evoluteal.Calculator.Business.Domain.Service
{
    public class RootDivResponse
    {
        public double Quotient { get; set; }
        public double Remainder { get; set; }
    }
}
