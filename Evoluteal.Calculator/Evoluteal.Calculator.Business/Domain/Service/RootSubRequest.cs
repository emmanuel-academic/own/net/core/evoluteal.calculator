﻿namespace Evoluteal.Calculator.Business.Domain.Service
{
    public class RootSubRequest
    {
        public double Minuend { get; set; }
        public double Subtrahend { get; set; }
    }
}
