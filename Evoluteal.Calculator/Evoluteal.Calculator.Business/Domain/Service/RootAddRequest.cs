﻿using System.Collections.Generic;

namespace Evoluteal.Calculator.Business.Domain.Service
{
    public class RootAddRequest
    {
        public List<double> Addends { get; set; }
    }
}
