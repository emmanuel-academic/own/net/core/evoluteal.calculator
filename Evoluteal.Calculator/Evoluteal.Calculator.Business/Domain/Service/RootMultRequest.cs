﻿using System.Collections.Generic;

namespace Evoluteal.Calculator.Business.Domain.Service
{
    public class RootMultRequest
    {
        public List<double> Factors { get; set; }
    }
}
