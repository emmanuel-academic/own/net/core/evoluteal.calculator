﻿namespace Evoluteal.Calculator.Business.Domain.Service
{
    public class RootSubResponse
    {
        public double Difference { get; set; }
    }
}
