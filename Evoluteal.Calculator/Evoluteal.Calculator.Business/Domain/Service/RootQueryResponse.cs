﻿using Evoluteal.Calculator.Business.Domain.TO;
using System.Collections.Generic;

namespace Evoluteal.Calculator.Business.Domain.Service
{
    public class RootQueryResponse
    {
        public List<OperationTO> Operations { get; set; }
    }
}
