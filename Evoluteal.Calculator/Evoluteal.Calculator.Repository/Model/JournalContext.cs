﻿using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using Microsoft.Extensions.Options;
using Evoluteal.Calculator.Common.Infrastructure;

namespace RepositoryData.Models
{
    public class JournalContext : DbContext
    {
        private readonly IOptions<Settings> _settings;

        public JournalContext(IOptions<Settings> settings)
        {
            this._settings = settings;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(this._settings.Value.ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblOperation>()
              .Property(b => b.Id)
              .HasMaxLength(7);
        }

        public DbSet<TblOperation> Operations { get; set; }
    }

    [Table("TblOperation")]
    public partial class TblOperation
    {
        [Key]
        [IgnoreDataMember]
        public int IdPK { get; set; }
        [IgnoreDataMember]
        [Required]
        public string Id { get; set; }
        [Required]
        public string Operation { get; set; }
        [Required]
        public string Calculation { get; set; }
        [Required]
        public DateTime Date { get; set; }
    }
}
